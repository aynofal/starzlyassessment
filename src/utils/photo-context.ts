import {createContext, useContext} from 'react';
import {PhotoAction} from '../reducers/photo-reducer';

interface PhotoContextProps {
  likedPhotos: Set<number>;
  dispatchToLikedPhotos: (action: PhotoAction) => void;
}

const PhotoContext = createContext<PhotoContextProps>({} as PhotoContextProps);

export const PhotoProvider = PhotoContext.Provider;

export const usePhoto = () => useContext(PhotoContext);
