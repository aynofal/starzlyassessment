/**
 * @author Ammar Nofal <aynofal@gmail.com>
 *
 * A react native app made for Starzly Interview Test
 *
 * This application is made as a single page app due to the requirement provided.
 */

import React, {useCallback, useEffect, useState} from 'react';
import {
  ActivityIndicator,
  FlatList,
  SafeAreaView,
  StatusBar,
  StyleSheet,
  useColorScheme,
  View,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';

import {_getPhotos} from './src/utils/api';
import Photo from './src/components/photo';
import {SPACING} from './src/utils/styling';
import {setLikedPhotos, usePhotoReducer} from './src/reducers/photo-reducer';
import {PhotoProvider} from './src/utils/photo-context';
import AsyncStorage from '@react-native-async-storage/async-storage';

// A result limiter, 20 is an acceptable real life use case number.
const PAGE_LENGTH = 20;

const App = () => {
  const isDarkMode = useColorScheme() === 'dark';
  const [photos, setPhotos] = useState<Photo[]>([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [hasMore, setHasMore] = useState(true);
  const [loadMore, setLoadMore] = useState(true);

  // Setup reducer
  const [likedPhotos, dispatchToLikedPhotos] = usePhotoReducer();

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    flex: 1,
  };

  // Init App Effect
  useEffect(() => {
    AsyncStorage.getItem('@STARZLY/LIKED_PHOTOS').then(res => {
      if (res) {
        dispatchToLikedPhotos(setLikedPhotos(JSON.parse(res)));
      }
    });
  }, []);

  // This effect will toggle whenever we want to load more photos, to avoid multiple fetches of the same items
  useEffect(() => {
    if (loadMore) {
      // Check if the API has more data first.
      if (hasMore) {
        _getPhotos(currentPage * PAGE_LENGTH, PAGE_LENGTH)
          .then(res => {
            // Check if result !null
            if (res) {
              setPhotos(prev => [...prev, ...res]);
              // If results of current page are less than the set length, this becomes our last page, avoid unnecessary API calls
              if (res.length < PAGE_LENGTH) {
                setHasMore(false);
              } else {
                setCurrentPage(prev => prev + 1);
              }
            }
          })
          .finally(() => setLoadMore(false));
      }
    }
  }, [currentPage, hasMore, loadMore]);

  const renderPhoto = useCallback(({item}) => {
    return <Photo {...item} />;
  }, []);

  return (
    <PhotoProvider value={{likedPhotos, dispatchToLikedPhotos}}>
      <SafeAreaView style={backgroundStyle}>
        <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
        <FlatList
          data={photos}
          renderItem={renderPhoto}
          onEndReached={() => setLoadMore(true)}
          onEndReachedThreshold={0.1}
          contentInsetAdjustmentBehavior="automatic"
          ItemSeparatorComponent={() => (
            <View
              style={{
                ...styles.separator,
                backgroundColor: isDarkMode ? Colors.light : Colors.dark,
              }}
            />
          )}
          keyExtractor={item => item.id.toString()}
          extraData={likedPhotos}
        />
        {loadMore ? <ActivityIndicator /> : null}
      </SafeAreaView>
    </PhotoProvider>
  );
};

const styles = StyleSheet.create({
  separator: {
    opacity: 0.2,
    marginHorizontal: SPACING[0],
    marginVertical: SPACING[2],
    height: 1,
  },
});

export default App;
