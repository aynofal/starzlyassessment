import axios, {AxiosResponse} from 'axios';

const BASE_URL = 'https://jsonplaceholder.typicode.com';

const QUERY = (start: number, limit: number): string =>
  `photos?_start=${start}&_limit=${limit}`;

const api = axios.create({
  baseURL: BASE_URL,
});

// Client interceptor to parse the error from Axios client and throw it.
api.interceptors.response.use(res => {
  if (res.data.status === false) {
    throw new Error(res.data.message);
  }
  return res;
});

export const _getPhotos = async (
  start = 0,
  limit = 2000,
): Promise<Photo[] | null> => {
  try {
    // If the response is not of Type Axios Response, an error is thrown, so this step is guaranteed.
    const res: AxiosResponse<Photo[]> = await api.get(`${QUERY(start, limit)}`);
    return res.data;
  } catch (e) {
    console.warn('Axios Error:', e);
    return null;
  }
};
