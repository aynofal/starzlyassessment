# Photo Library System
Implemented Feature:
- Photo feed with lazy loading
- Ability to like/unlike a photo
- Persistence of likes and dislikes
- State Management via the useReducer hook for likes management (using redux is not suitable in this scope as it increases boilerplate code for a small-scale project).

Additional Notes:
- Project has been tested on iOS and Android
- Built manually without CI/CD tools, but can be built with scripts if so required (using Fastlane)
- This is a single page app as all required info is available on the feed, an improvement I could think of is adding grouping by album ID, given the one day requirement mentioned in the doc, I went ahead without that concept.
- The lazy loading library had some issues on Android so I made some minor tweaks noticeable in the photo component.
