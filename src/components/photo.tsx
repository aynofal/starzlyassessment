import React, {FC, useCallback, useMemo} from 'react';
import ProgressiveFastImage from '@freakycoder/react-native-progressive-fast-image';
import {
  View,
  Dimensions,
  StyleSheet,
  Text,
  useColorScheme,
  TouchableOpacity,
  Image,
  Platform,
} from 'react-native';
import {SPACING} from '../utils/styling';
import {Colors} from 'react-native/Libraries/NewAppScreen';
// @ts-ignore
import LIKE_ICON from '../assets/icons/like-icon.png';
// @ts-ignore
import LOADING from '../assets/loading2.gif';
import {usePhoto} from '../utils/photo-context';
import {likePhoto, unlikePhoto} from '../reducers/photo-reducer';

interface PhotoProps extends Photo {}

const {width} = Dimensions.get('window');

const IMG_WIDTH = width;
const IMG_HEIGHT = IMG_WIDTH;

const Photo: FC<PhotoProps> = ({id, thumbnailUrl, title, url}) => {
  const isDarkMode = useColorScheme() === 'dark';
  const {likedPhotos, dispatchToLikedPhotos} = usePhoto();
  const isLiked = useMemo(() => likedPhotos.has(id), [likedPhotos]);

  const textColor = useMemo(
    () => ({
      color: isDarkMode ? Colors.light : Colors.darker,
    }),
    [isDarkMode],
  );
  const likeIconColor = useMemo(
    () => ({
      tintColor: isLiked ? '#01bde0' : isDarkMode ? '#fff' : '#a9a9a9',
    }),
    [isLiked, isDarkMode],
  );

  const toggleLike = useCallback(() => {
    dispatchToLikedPhotos(isLiked ? unlikePhoto(id) : likePhoto(id));
  }, [isLiked]);

  return (
    <View style={styles.contentContainer}>
      <ProgressiveFastImage
        style={styles.photo}
        thumbnailSource={{uri: Platform.OS === 'ios' ? thumbnailUrl : url}}
        source={{uri: url}}
        blurRadius={Platform.OS === 'ios' ? 3 : 0}
        loadingImageStyle={{
          width: IMG_WIDTH,
          height: IMG_HEIGHT,
          alignSelf: 'center',
        }}
        loadingSource={LOADING}
      />
      <View style={styles.actionContainer}>
        <TouchableOpacity onPress={toggleLike}>
          <Image source={LIKE_ICON} style={[styles.likeIcon, likeIconColor]} />
        </TouchableOpacity>
      </View>
      <View style={styles.titleContainer}>
        <Text style={[styles.title, textColor]}>{title}</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  contentContainer: {
    width,
  },
  photo: {
    width: IMG_WIDTH,
    height: IMG_HEIGHT,
    marginBottom: SPACING[1],
  },
  titleContainer: {
    marginHorizontal: SPACING[1],
  },
  title: {
    fontWeight: 'bold',
    fontSize: 16,
  },
  actionContainer: {
    marginVertical: SPACING[0],
    marginHorizontal: SPACING[1],
    flexDirection: 'row',
  },
  likeIcon: {
    width: SPACING[3],
    height: SPACING[3],
    resizeMode: 'contain',
  },
});

export default Photo;
