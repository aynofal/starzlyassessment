import {useReducer} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

type PhotoReducerAction = 'LIKE_PHOTO' | 'UNLIKE_PHOTO' | 'SET_LIKED_PHOTOS';

export interface PhotoAction {
  type: PhotoReducerAction;
  payload?: number | number[];
}

// Action Generators
export function likePhoto(payload: number): PhotoAction {
  return {
    type: 'LIKE_PHOTO',
    payload,
  };
}
export function unlikePhoto(payload: number): PhotoAction {
  return {
    type: 'UNLIKE_PHOTO',
    payload,
  };
}
export function setLikedPhotos(payload: number[]): PhotoAction {
  return {
    type: 'SET_LIKED_PHOTOS',
    payload,
  };
}

function persistData(state: Set<number>) {
  AsyncStorage.setItem(
    '@STARZLY/LIKED_PHOTOS',
    JSON.stringify(Array.from(state)),
  );
}

// Initial state of the app.
const initialState: Set<number> = new Set<number>();

function reducer(state: Set<number>, action: PhotoAction) {
  const {type, payload} = action;
  switch (type) {
    case 'LIKE_PHOTO':
      if (payload && typeof payload === 'number' && !state.has(payload)) {
        state.add(payload);
        persistData(state);
      }
      return new Set(state);
    case 'UNLIKE_PHOTO':
      if (payload && typeof payload === 'number') {
        state.delete(payload);
        persistData(state);
      }
      return new Set(state);
    case 'SET_LIKED_PHOTOS':
      if (payload && Array.isArray(payload)) {
        return new Set(payload);
      }
      return state;
    default:
      throw new Error('Undefined Action');
  }
}

export const usePhotoReducer = () => useReducer(reducer, initialState);
